package br.com.omni.openbaking;

import br.com.omni.arquitetura.starters.logs.amqp.EnableRabbitMQSaveLogTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableRabbitMQSaveLogTransaction
public class OpenBankingApplication {

    public static void main(String[] args) { //NOSONAR
        SpringApplication.run(OpenBankingApplication.class, args);
    }

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
    }
}