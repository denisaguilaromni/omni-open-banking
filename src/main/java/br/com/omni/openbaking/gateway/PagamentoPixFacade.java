package br.com.omni.openbaking.gateway;

import br.com.omni.ib.starter.domain.request.CreatePixPaymentRequest;
import br.com.omni.ib.starter.domain.response.PixPaymentResponse;
import org.springframework.http.ResponseEntity;

public interface PagamentoPixFacade {

    ResponseEntity<PixPaymentResponse> criar(String authorization, String xFapiAuthDate, String xFapiCustomerIpAddress,
                                             String xFapiInteractionId, String xCustomerUserAgent, String xIdempotencyKey,
                                             String xJwsSignature, CreatePixPaymentRequest request);

    ResponseEntity<PixPaymentResponse> consultar(String paymentId, String authorization, String xFapiAuthDate,
                                                 String xFapiCustomerIpAddress, String xFapiInteractionId,
                                                 String xCustomerUserAgent, String xJwsSignature);
}
