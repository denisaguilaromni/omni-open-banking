package br.com.omni.openbaking.gateway.filter;

import br.com.omni.openbaking.config.filter.CorsProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpHeaders.*;

@Component
@RequiredArgsConstructor
public class DefaultHeaderProperties implements InitializingBean {

    protected static final String[] DEFAULT_HEADERS = {ACCESS_CONTROL_MAX_AGE, ACCESS_CONTROL_ALLOW_METHODS, ACCESS_CONTROL_ALLOW_ORIGIN, ACCESS_CONTROL_ALLOW_HEADERS};
    public static final String ACCESS_CONTROL_MAX_AGE_VALUE = "3600";
    public static final String ACCESS_CONTROL_ALLOW_METHODS_VALUE = "GET,POST,OPTIONS,PUT,PATCH,DELETE,TRACE"; //HEAD
    public static final String ACCESS_CONTROL_ALLOW_ORIGIN_VALUE = "*";
    private final Map<String, String> defaultHeadersMap = new HashMap<>();

    @Autowired
    private final CorsProperties gatewayProperties;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.defaultHeadersMap.put(ACCESS_CONTROL_MAX_AGE, ACCESS_CONTROL_MAX_AGE_VALUE);
        this.defaultHeadersMap.put(ACCESS_CONTROL_ALLOW_METHODS, ACCESS_CONTROL_ALLOW_METHODS_VALUE);
        this.defaultHeadersMap.put(ACCESS_CONTROL_ALLOW_ORIGIN, ACCESS_CONTROL_ALLOW_ORIGIN_VALUE);
        this.defaultHeadersMap.put(ACCESS_CONTROL_ALLOW_HEADERS, this.gatewayProperties.getCommaDelimitedHeaders().toUpperCase());
    }

    public String getDefaultHeaderValue(final String headerName) {
        return this.defaultHeadersMap.get(headerName);
    }

    public boolean isDefaultHeader(final String headerName) {
        return this.defaultHeadersMap.containsKey(headerName);
    }

    public Map<String, String> getDefaultHeaders() {
        return this.defaultHeadersMap;
    }
}