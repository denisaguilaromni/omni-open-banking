package br.com.omni.openbaking.gateway.client;

import br.com.omni.ib.starter.domain.request.CreatePixPaymentRequest;
import br.com.omni.ib.starter.domain.response.PixPaymentResponse;
import br.com.omni.ib.starter.http.PagamentoWS;
import br.com.omni.ib.starter.http.TransferenciaWS;
import br.com.omni.openbaking.gateway.PagamentoPixFacade;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PagamentoPixFacadeImpl implements PagamentoPixFacade {

    private PagamentoWS pagamentoWS;

    private TransferenciaWS transferenciaWS;

    @Override
    public ResponseEntity<PixPaymentResponse> criar(String authorization, String xFapiAuthDate, String xFapiCustomerIpAddress,
                                                    String xFapiInteractionId, String xCustomerUserAgent, String xIdempotencyKey,
                                                    String xJwsSignature, CreatePixPaymentRequest request) {

        return pagamentoWS.criar(authorization, xFapiAuthDate, xFapiCustomerIpAddress, xFapiInteractionId,
            xCustomerUserAgent, xIdempotencyKey, xJwsSignature, request);
    }

    @Override
    public ResponseEntity<PixPaymentResponse> consultar(String paymentId, String authorization, String xFapiAuthDate,
                                                        String xFapiCustomerIpAddress, String xFapiInteractionId,
                                                        String xCustomerUserAgent, String xJwsSignature) {

        return pagamentoWS.consultar(paymentId, authorization, xFapiAuthDate, xFapiCustomerIpAddress, xFapiInteractionId,
            xCustomerUserAgent, xJwsSignature);
    }
}
