package br.com.omni.openbaking.gateway.filter;

import br.com.omni.arquitetura.starters.logs.commons.gateway.filter.AuditFunctions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class CORSFilter implements Filter {

    public static final String MANAGEMENT_CONTEXT_PATH = "management.context-path";

    @Autowired
    private DefaultHeaderProperties headers;

    @Autowired
    Environment env;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        if (AuditFunctions.checkAllowPage((HttpServletRequest) req)) {

            chain.doFilter(req, res);

        } else{

            HttpServletResponse response = (HttpServletResponse) res;
            this.headers.getDefaultHeaders().forEach(response::setHeader);

            chain.doFilter(req, res);
        }
    }

}