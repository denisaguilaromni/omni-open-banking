package br.com.omni.openbaking.gateway.filter;

import br.com.omni.arquitetura.starters.logs.commons.gateway.filter.AuditFunctions;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpMethod.OPTIONS;

@Component
@Order(HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class MethodOptionsCORSFilter extends OncePerRequestFilter {

	public static final String MANAGEMENT_CONTEXT_PATH = "management.context-path";

	/**
     * The Default header values.
	 */
	private final DefaultHeaderProperties defaultHeaderValues;

	private final Environment env;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws ServletException, IOException {
		if (AuditFunctions.checkAllowPage(request)) {
			chain.doFilter(request, response);
		} else{
			if (request.getMethod().equals(OPTIONS.name())) {
				this.defaultHeaderValues.getDefaultHeaders().forEach(response::setHeader);
			} else {
				chain.doFilter(request, response);
			}
		}
	}

}