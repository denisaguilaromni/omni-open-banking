package br.com.omni.openbaking.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@Profile({"local", "hmg"})
public class SwaggerConfig {

    private static final String TITLE = "OpenBankingApplication APIs";
    private static final String VERSION = "1.0";
    private static final String PATHS_API = "/api/.*"; //NOSONAR
    public static final String APP_SWAGGER_HOST = "app.swagger.host";

    @Bean
    public Docket documentation(Environment env) {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(env.getRequiredProperty(APP_SWAGGER_HOST))
                .select()
                    .apis(RequestHandlerSelectors.any())
                    .paths(regex(PATHS_API))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(TITLE).version(VERSION).build();
    }

}