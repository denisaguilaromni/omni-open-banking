package br.com.omni.openbaking.http;

public final class URLMapping {

    private URLMapping() {
    }

    public static final String ROOT_API_PATH = "/api"; //NOSONAR

    public static final String ROOT_API_PAGAMENTOS = ROOT_API_PATH + "/payments";
}
