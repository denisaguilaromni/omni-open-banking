package br.com.omni.openbaking.http;

import br.com.omni.ib.starter.domain.request.CreatePixPaymentRequest;
import br.com.omni.ib.starter.domain.response.PixPaymentResponse;
import br.com.omni.openbaking.gateway.PagamentoPixFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static br.com.omni.openbaking.http.RequestProperties.HEADER_AUTHORIZATION;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_CUSTOMER_USER_AGENT;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_FAPI_AUTH_DATE;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_FAPI_CUSTOMER_IP_ADDRESS;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_FAPI_INTERACTION_ID;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_IDEMPOTENCY_KEY;
import static br.com.omni.openbaking.http.RequestProperties.HEADER_X_JWS_SIGNATURE;
import static br.com.omni.openbaking.http.RequestProperties.PATH_PAYMENT_ID;
import static br.com.omni.openbaking.http.URLMapping.ROOT_API_PAGAMENTOS;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@AllArgsConstructor
@RequestMapping(ROOT_API_PAGAMENTOS)
@Api(tags = "Pagamentos", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class PagamentoController {

    private PagamentoPixFacade pagamentoPixFacade;

    @PostMapping
    @ApiOperation(value = "Criar uma iniciação de pagamento")
    public ResponseEntity<PixPaymentResponse> criar(@RequestHeader(HEADER_AUTHORIZATION) String authorization,
                                                    @RequestHeader(value = HEADER_X_FAPI_AUTH_DATE, required = false) String xFapiAuthDate,
                                                    @RequestHeader(value = HEADER_X_FAPI_CUSTOMER_IP_ADDRESS, required = false) String xFapiCustomerIpAddress,
                                                    @RequestHeader(value = HEADER_X_FAPI_INTERACTION_ID, required = false) String xFapiInteractionId,
                                                    @RequestHeader(value = HEADER_X_CUSTOMER_USER_AGENT, required = false) String xCustomerUserAgent,
                                                    @RequestHeader(HEADER_X_IDEMPOTENCY_KEY) String xIdempotencyKey,
                                                    @RequestHeader(HEADER_X_JWS_SIGNATURE) String xJwsSignature,
                                                    @RequestBody CreatePixPaymentRequest request) {

        return pagamentoPixFacade.criar(authorization, xFapiAuthDate, xFapiCustomerIpAddress, xFapiInteractionId,
            xCustomerUserAgent, xIdempotencyKey, xJwsSignature, request);
    }

    @GetMapping(path = "/{paymentId}")
    @ApiOperation(value = "Criar uma conulta de pagamento")
    public ResponseEntity<PixPaymentResponse> consultar(@PathVariable @ApiParam(PATH_PAYMENT_ID) String paymentId,
                                                        @RequestHeader(HEADER_AUTHORIZATION) String authorization,
                                                        @RequestHeader(value = HEADER_X_FAPI_AUTH_DATE, required = false) String xFapiAuthDate,
                                                        @RequestHeader(value = HEADER_X_FAPI_CUSTOMER_IP_ADDRESS, required = false) String xFapiCustomerIpAddress,
                                                        @RequestHeader(value = HEADER_X_FAPI_INTERACTION_ID, required = false) String xFapiInteractionId,
                                                        @RequestHeader(value = HEADER_X_CUSTOMER_USER_AGENT, required = false) String xCustomerUserAgent,
                                                        @RequestHeader(HEADER_X_JWS_SIGNATURE) String xJwsSignature) {

        return pagamentoPixFacade.consultar(paymentId, authorization, xFapiAuthDate, xFapiCustomerIpAddress, xFapiInteractionId, xCustomerUserAgent, xJwsSignature);
    }
}
