package br.com.omni.openbaking.http;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class RequestProperties {

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_X_FAPI_AUTH_DATE = "x-fapi-auth-date";
    public static final String HEADER_X_FAPI_CUSTOMER_IP_ADDRESS = "x-fapi-customer-ip-address";
    public static final String HEADER_X_FAPI_INTERACTION_ID = "x-fapi-interaction-id";
    public static final String HEADER_X_CUSTOMER_USER_AGENT = "x-customer-user-agent";
    public static final String HEADER_X_IDEMPOTENCY_KEY = "x-idempotency-key";
    public static final String HEADER_X_JWS_SIGNATURE = "x-jws-signature";

    public static final String PATH_PAYMENT_ID = "paymentId";
}
