package br.com.omni.openbaking.http;

import br.com.omni.commons.domain.Autenticacao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static net.logstash.logback.argument.StructuredArguments.value;

@Slf4j
public final class BaseWS {

    private BaseWS(){}

    public static final String INTERNAL_MESSAGE_ERROR = "Internal Process Error. Check the data. This error had this transactionID ";
    public static final String LOGIN_USER_TOKEN_EXAMPLE = "{\"login\":\"USER\", \"token\":\"123\", \"integracaoLogin\":\"USER\"}";
    public static final String LOGIN_USER_TOKEN_AGENTE_EXAMPLE = "{\"login\":\"USER\", \"token\":\"123\", \"agente\":1234, \"integracaoLogin\":\"USER\"}";

    public static Autenticacao getPrincipal(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (Autenticacao) authentication.getPrincipal();
    }
}
