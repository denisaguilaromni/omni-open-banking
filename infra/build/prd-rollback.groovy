#!/usr/bin/env groovy

@Library('omni-infra-java-jenkins-library')_

import groovy.transform.Field
import br.com.omni.pipeline.Pipeline
import br.com.omni.pipeline.PipeFactory
import br.com.omni.pipeline.JavaRuntime
import static br.com.omni.pipeline.PipeType.JAVA_PRD
import static br.com.omni.pipeline.JavaRuntime.OPENJDK_11

@Field def appName = 'omni-open-banking'
@Field def JavaRuntime appRuntime = OPENJDK_11
@Field def gitUrl = 'https://bitbucket.org/omnifinanceira/omni-open-banking.git'

Pipeline pipe = PipeFactory.create(JAVA_PRD, this)
pipe.rollback("${RELEASE_VERSION}")
