variable "aws_region" {
  type    = string
  default = "sa-east-1"
}

variable "aws_owner" {
  type    = string
  default = "199142183140"
}

variable "app_name" {
  type    = string
  default = "omni-open-banking"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "ami_search_pattern" {
  type    = string
  default = "dev-packer-aws-ubuntu-1804-omni-open-banking-*"
}

variable "dns_update_aim_policy_arn" {
  type    = string
  default = "arn:aws:iam::199142183140:policy/OmniAwsLocalPrivateZoneDynamicDNSUpdatePolicy"
}

variable "autenticacao_starter_aim_policy_arn" {
  type    = string
  default = "arn:aws:iam::199142183140:policy/DevOmniAutententicacaoStarterPolicy"
}