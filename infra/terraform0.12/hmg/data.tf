# AMI search
data "aws_ami" "app" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_search_pattern]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [var.aws_owner]
}

# omniaws.local VPC
data "terraform_remote_state" "omniaws-vpc" {
  backend = "s3"

  config = {
    region = var.aws_region
    bucket = "tfstateomniaws"
    key    = "omniaws-vpc/terraform.tfstate"
  }
}

# VPC's private subnets
data "aws_subnet_ids" "private_subnets" {
  vpc_id = data.terraform_remote_state.omniaws-vpc.outputs.vpc_id

  tags = {
    Name = "${var.environment}-omniaws-private-subnet-1*"
  }
}

# Security Groups
data "terraform_remote_state" "omniaws-sg" {
  backend = "s3"

  config = {
    region = var.aws_region
    bucket = "tfstateomniaws"
    key    = "omniaws-sg/terraform.tfstate"
  }
}

# route53 zones
data "aws_route53_zone" "external" {
  name = "omni.com.br."
}

data "aws_route53_zone" "internal" {
  name         = "omniaws.local."
  private_zone = true
}

# AWS ALB and Listener Rules
data "aws_lb" "alb" {
  name = "${var.environment}-alb"
}

data "aws_lb_listener" "external_listener" {
  load_balancer_arn = "${data.aws_lb.alb.arn}"
  port              = 443
}

data "aws_lb" "alb_internal" {
  name = "${var.environment}-alb-internal"
}

data "aws_lb_listener" "internal_listener" {
  load_balancer_arn = "${data.aws_lb.alb_internal.arn}"
  port              = 80
}

data "aws_lb" "alb_healthcheck" {
  name = "${var.environment}-alb-healthcheck"
}

data "aws_lb_listener" "healthcheck_listener" {
  load_balancer_arn = "${data.aws_lb.alb_healthcheck.arn}"
  port              = 80
}

# AWS Availability Zones
data "aws_availability_zones" "available" {
  state = "available"
}
