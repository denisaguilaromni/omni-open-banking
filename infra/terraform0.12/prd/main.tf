# To initialize backend: terraform init -backend-config=backend.hcl

terraform {
  backend "s3" {
    key = "prd/omni-open-banking/terraform.tfstate"
  }
}

# AWS provider
provider "aws" {
  version = "= 2.33.0"
  region  = var.aws_region
}

# template
provider "template" {
  version = "= 2.1.2"
}

# Cloudflame
provider "cloudflare" {
  version = "= 2.3"
}

# app module
module "app" {
  source = "git::git@bitbucket.org:/omnifinanceira/terraform-aws-app.git?ref=v0.1.0"

  # Environment
  # Example: hmg
  # Type: string
  environment = var.environment

  # app_name
  # The name of Application to be used in combination with environment value.
  # Must be the name of the Git repository
  # The final result should be: ${var.environment}-${var.app_name}
  # Must be the name of the Git repository: omni-open-banking for git@bitbucket/omnifinanceira/omni-open-banking.git
  # Example: omni-open-banking
  # Type: string
  app_name = var.app_name

  # EC2 Instance types
  # Type: list of string
  instance_types = ["t3.small", "t2.small", "t2.medium", "t3.medium"]

  # Mixed Instances Policy
  # Types: number
  on_demand_base_capacity                  = 1
  on_demand_percentage_above_base_capacity = 0

  # ECS Allocation Strategy:
  # Doc: https://docs.aws.amazon.com/autoscaling/ec2/userguide/asg-purchase-options.html#asg-allocation-strategies
  # Type: string
  spot_allocation_strategy = "capacity-optimized"

  # spot_instance_pools MUST BE used when spot_allocation_strategy variable have "lowest-price" value.
  # Default: 0
  # Type: number
  # spot_instance_pools = 0

  # Autoscaling Group
  min_size           = 2
  max_size           = 4
  enable_autoscaling = true

  # Autoscaling Healh Check
  health_check_type         = "ELB"
  health_check_grace_period = 180

  # EBS
  # ebs_volume_size = 8
  # ebs_volume_type = "standard"

  # AMI to use within ASG
  # default: data.aws_ami.app.id
  # CLI example: terraform apply -var="ami=ami-abc123"
  # CI/CD example: erraform apply -var="ami_search_pattern=packer-aws-ubuntu-1804-openjdk-jre-8-headless-app-*"
  # Type: string
  ami = data.aws_ami.app.id

  # VPC Id
  vpc_id = data.terraform_remote_state.omniaws-vpc.outputs.vpc_id

  # Subnet Ids
  # Type: list of string
  subnet_ids = data.aws_subnet_ids.private_subnets.ids

  # Availabilty Zones
  # Type: list of string
  availability_zones = data.aws_availability_zones.available.names

  # Security Groups List
  # Type: list of string
  security_groups = [data.terraform_remote_state.omniaws-sg.outputs.java, data.terraform_remote_state.omniaws-sg.outputs.ssh-int]

  # ALB configs

  # To enable external access enable_external_access must be true
  enable_external_access     = false
  external_alb_listener_arn  = data.aws_lb_listener.external_listener.arn
  external_route53_zone_id   = data.aws_route53_zone.external.zone_id
  external_route53_records   = [data.aws_lb.alb.dns_name]
  external_route53_zone_name = replace(data.aws_route53_zone.external.name, "/[.]$/", "")

  # To enable internal access enable_internal_access must be true
  enable_internal_access     = true
  internal_alb_listener_arn  = data.aws_lb_listener.internal_listener.arn
  internal_route53_zone_id   = data.aws_route53_zone.internal.zone_id
  internal_route53_records   = [data.aws_lb.alb_internal.dns_name]
  internal_route53_zone_name = replace(data.aws_route53_zone.internal.name, "/[.]$/", "")

  # To enable healthcheck access enable_healthcheck_access must be true
  enable_healthcheck_access     = false
  healthcheck_alb_listener_arn  = data.aws_lb_listener.healthcheck_listener.arn
  healthcheck_route53_zone_id   = data.aws_route53_zone.internal.zone_id
  healthcheck_route53_records   = [data.aws_lb.alb_healthcheck.dns_name]
  healthcheck_route53_zone_name = replace(data.aws_route53_zone.internal.name, "/[.]$/", "")

  # Custom Route53 Alias
  enable_custom_route53_alias = false
  custom_route53_alias        = "${var.environment}-omni-open-banking-custom"

  # Required if enable_external_access or enable_internal_access are set to true
  # Max num of chars are 24 because target group name cannot be longer than 32 characters
  # name = env-${target_group_name}-ext or env-${target_group_name}-int, where env is equal to hmg or prd
  # Type: string
  target_group_name        = var.app_name
  target_group_health_path = "/manager/health"
  target_group_health_port = 8081

  # Update Route53 record set with EC2 instance private IP
  dns_update_aim_policy_arn = var.dns_update_aim_policy_arn

  # omni-autenticacao-starter IAM Profile's Policy
  autenticacao_starter_aim_policy_arn = var.autenticacao_starter_aim_policy_arn

}
